## **AWS ECS**
> **Definitions**: ECS – Elastic Container Service,a highly scalable, fast, container management service that makes it easy to run / stop / manage Docker containers on a cluster.
>### *ECS Objects:*
>- Task Definition
>>- Include container's image and container level setting (eg: image, port, registry, Enviroment varibles, ...)
>- Cluster
>>- Same like *Docker-compose* file, a **blue print** for application and describle one / more container through attributes.
>>- Few attributes are configured at this level, it's prefer to be configured at **container level**.
>>- This's a combination of multiple container definitions if we're using more than one container in a Task *(aka: a node in DockerSwarn)*.
>- Service
>>- This allow we to run and maintain a **desired number** of instances running application in a Task definition.
>- Task
>>- This is a part of a task definition within a cluster.
>>- After a task definition is **created**, only then can we define tasks that will run on the cluster.
>>- If using **Fargate** ECS type, then it has its own **isolation boundary** and **does not** share the underlying kernel, CPU resources, memory resources, or elastic network interface with another task.

![ECS Layers](./Image/Screenshot%20from%202022-09-27%2010-05-16.png)

>### *ECS types:*
>>- Fargate (Serverless)
>>> AWS Fargate pricing is calculated based on the vCPU, memory, Operating Systems, CPU Architecture, and storage resources used from the time you start to download your container image until the Amazon ECS Task or Amazon EKS2 Pod terminates, rounded up to the nearest second.
>>- EC2 with Linux
>>- EC2 with Windows

## **AWS ECR**
> **Definition**: Elastic Container Registry (ECR) is a fully-managed Docker container registry that makes it easy for developers to store, manage, and deploy Docker container images.

> **Benifits**:
>>- Full managed
>>- Secure (using Cretificates / SSH key / ...)
>>- High Avaible
>>- Simplified Flow

![How ECR Work](./Image/Screenshot%20from%202022-09-27%2010-14-02.png)
## **AWS EKS**