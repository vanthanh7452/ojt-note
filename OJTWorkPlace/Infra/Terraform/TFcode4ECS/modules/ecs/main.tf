resource "aws_ecr_repository" "ecr" {
  name = "${var.ecr_name}"
  tags = {
    "Name" = "${var.ecr_name}"
    Environment = "${var.environment}"
  }
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.ecs_cluster_name}"
  tags = {
    "Name" = "${var.ecs_cluster_name}"
    Environment = "${var.environment}"
  }
}

resource "aws_ecs_task_definition" "ecs_task_definition" {
  family = "${var.ecs_task_definition_name}"
  container_definitions = <<DEFINITION
[
  {
    "name": "${var.ecs_task_definition_name}",
    "image": "${var.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.nginx_image_name}",
    "cpu": 256,
    "memory": 512,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${var.ecs_task_definition_name}",
        "awslogs-region": "${var.region}",
        "awslogs-create-group": "true",
        "awslogs-stream-prefix": "ecs"
      }
    }
  }
]
DEFINITION
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  cpu = "512"
  memory = "1024"
  execution_role_arn = "${aws_iam_role.ecs_task_execution_role.arn}"
  task_role_arn = "${aws_iam_role.ecs_task_execution_role.arn}"
  tags = {
    "Name" = "${var.ecs_task_definition_name}"
    Environment = "${var.environment}"
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "${var.ecs_task_execution_role_name}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  tags = {
    "Name" = "${var.ecs_task_execution_role_name}"
    Environment = "${var.environment}"
  }
}

resource "aws_iam_role_policy" "ecs_task_execution_role_policy" {
  name = "${var.ecs_task_execution_role_policy_name}"
  role = "${aws_iam_role.ecs_task_execution_role.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ecr:GetAuthorizationToken",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetRepositoryPolicy",
        "ecr:DescribeRepositories",
        "ecr:ListImages",
        "ecr:BatchGetImage",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ecs_task_execution_role_policy_cloudwatch" {
  name = "${var.ecs_task_execution_role_policy_cloudwatch_name}"
  role = "${aws_iam_role.ecs_task_execution_role.id}"
  policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
    {
        "Action": [
            "autoscaling:Describe*",
            "cloudwatch:*",
            "logs:*",
            "sns:*",
            "iam:GetPolicy",
            "iam:GetPolicyVersion",
            "iam:GetRole"
        ],
        "Effect": "Allow",
        "Resource": "*"
    },
    {
        "Effect": "Allow",
        "Action": "iam:CreateServiceLinkedRole",
        "Resource": "arn:aws:iam::*:role/aws-service-role/events.amazonaws.com/AWSServiceRoleForCloudWatchEvents*",
        "Condition": {
            "StringLike": {
                "iam:AWSServiceName": "events.amazonaws.com"
            }
        }
    }
]
}
EOF
}



resource "aws_ecs_service" "ecs_service" {
  name = "${var.ecs_service_name}"
  cluster = "${aws_ecs_cluster.ecs_cluster.id}"
  task_definition = "${aws_ecs_task_definition.ecs_task_definition.family}:${aws_ecs_task_definition.ecs_task_definition.revision}"
  desired_count = 1
  launch_type = "FARGATE"
  network_configuration {
    subnets = ["${var.public_subnet_1_id}", "${var.public_subnet_2_id}"]
    security_groups = ["${var.security_group_id}"]
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = "${aws_lb_target_group.lb_target_group_1.arn}"
    container_name = "${var.ecs_task_definition_name}"
    container_port = 80
  }
  load_balancer {
    target_group_arn = "${aws_lb_target_group.lb_target_group_2.arn}"
    container_name = "${var.ecs_task_definition_name}"
    container_port = 80
  }
  deployment_controller {
    type = "CODE_DEPLOY"
  }
  tags = {
    "Name" = "${var.ecs_service_name}"
    Environment = "${var.environment}"
  }
  depends_on = [aws_lb_listener.lb_listener]
}

resource "aws_lb_target_group" "lb_target_group_1" {
  name = "${var.lb_target_group_name_1}"
  port = 80
  protocol = "HTTP"
  vpc_id = "${var.vpc_id}"
  target_type = "ip"
  deregistration_delay = 10
  tags = {
    "Name" = "${var.lb_target_group_name_1}"
    Environment = "${var.environment}"
  }
}

resource "aws_lb_target_group" "lb_target_group_2" {
  name = "${var.lb_target_group_name_2}"
  port = 80
  protocol = "HTTP"
  vpc_id = "${var.vpc_id}"
  target_type = "ip"
  deregistration_delay = 10
  tags = {
    "Name" = "${var.lb_target_group_name_2}"
    Environment = "${var.environment}"
  }
}

resource "aws_lb" "lb" {
  name = "${var.lb_name}"
  internal = false
  load_balancer_type = "application"
  security_groups = ["${var.security_group_id}"]
  subnets = ["${var.public_subnet_1_id}", "${var.public_subnet_2_id}"]
  tags = {
    "Name" = "${var.lb_name}"
    Environment = "${var.environment}"
  }
}

resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn = "${aws_lb.lb.arn}"
  port = "80"
  protocol = "HTTP"
  default_action {
    type = "forward"
    forward {
      target_group {
        arn = "${aws_lb_target_group.lb_target_group_1.arn}"
        weight = 100
      }
      target_group {
        arn = "${aws_lb_target_group.lb_target_group_2.arn}"
        weight = 0
      }
      stickiness {
        enabled  = false
        duration = 1
      }
    }
  }
}

resource "aws_lb_listener_rule" "lb_listener_rule" {
  listener_arn = "${aws_lb_listener.lb_listener.arn}"
  priority = 1
  action {
    type = "forward"
    forward {
      target_group {
        arn = "${aws_lb_target_group.lb_target_group_1.arn}"
      }
      target_group {
        arn = "${aws_lb_target_group.lb_target_group_2.arn}"
      }
    }
  }
  condition {
    path_pattern {
      values = ["/"]
    }
  }
}
# https://www.terraform.io/docs/providers/aws/r/codedeploy_app.html
resource "aws_codedeploy_app" "codedeploy_app" {
  name = "${var.codedeploy_app_name}"
  compute_platform = "${var.codedeploy_compute_platform}"
}

# https://www.terraform.io/docs/providers/aws/r/codedeploy_deployment_group.html
resource "aws_codedeploy_deployment_group" "codedeploy_deployment_group" {
  app_name = "${var.codedeploy_app_name}"
  deployment_config_name = "CodeDeployDefault.ECSAllAtOnce"
  deployment_group_name = "${var.deployment_group_name}"
  service_role_arn = "${aws_iam_role.codedeploy_IAM_role.arn}"

  # You can configure a deployment group or deployment to automatically roll back when a deployment fails or when a
  # monitoring threshold you specify is met. In this case, the last known good version of an application revision is deployed.
  # https://docs.aws.amazon.com/codedeploy/latest/userguide/deployment-groups-configure-advanced-options.html
  auto_rollback_configuration {
    # If you enable automatic rollback, you must specify at least one event type.
    enabled = "${var.codedeploy_auto_rollback_enabled}"

    # The event type or types that trigger a rollback. Supported types are DEPLOYMENT_FAILURE and DEPLOYMENT_STOP_ON_ALARM.
    events = "${var.codedeploy_auto_rollback_events}"
  }

  # You can configure options for a blue/green deployment.
  # https://docs.aws.amazon.com/codedeploy/latest/APIReference/API_BlueGreenDeploymentConfiguration.html
  blue_green_deployment_config {
    # Information about how traffic is rerouted to instances in a replacement environment in a blue/green deployment.
    deployment_ready_option {
      # Information about when to reroute traffic from an original environment to a replacement environment in a blue/green deployment.
      #
      # - CONTINUE_DEPLOYMENT: Register new instances with the load balancer immediately after the new application
      #                        revision is installed on the instances in the replacement environment.
      # - STOP_DEPLOYMENT: Do not register new instances with a load balancer unless traffic rerouting is started
      #                    using ContinueDeployment. If traffic rerouting is not started before the end of the specified
      #                    wait period, the deployment status is changed to Stopped.
      action_on_timeout = "${var.codedeploy_deployment_ready_option_action_on_timeout}"

      # The number of minutes to wait before the status of a blue/green deployment is changed to Stopped
      # if rerouting is not started manually. Applies only to the STOP_DEPLOYMENT option for action_on_timeout.
      # Can not be set to STOP_DEPLOYMENT when timeout is set to 0 minutes.
      wait_time_in_minutes = "${var.codedeploy_deployment_ready_option_wait_time_in_minutes}"
    }

    # You can configure how instances in the original environment are terminated when a blue/green deployment is successful.
    terminate_blue_instances_on_deployment_success {
      # Valid values are TERMINATE or KEEP_ALIVE.
      # If specified TERMINATE, then instances are terminated after a specified wait time.
      # On the other hand, if specified KEEP_ALIVE, then occurred an unknown error when terraform apply.
      action                           = "TERMINATE"

      # The number of minutes to wait after a successful blue/green deployment before terminating instances
      # from the original environment. The maximum setting is 2880 minutes (2 days).
      termination_wait_time_in_minutes = "${var.codedeploy_terminate_blue_instances_on_deployment_success_termination_wait_time_in_minutes}"
    }
  }

  # For ECS deployment, the deployment type must be BLUE_GREEN, and deployment option must be WITH_TRAFFIC_CONTROL.
  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

  # Configuration block(s) of the ECS services for a deployment group.
  ecs_service {
    cluster_name = aws_ecs_cluster.ecs_cluster.name
    service_name = aws_ecs_service.ecs_service.name
  }
  # Configure the Load Balancer to use in a deployment.
  load_balancer_info {
    # Information about two target groups and how traffic routes during an Amazon ECS deployment.
    # An optional test traffic route can be specified.
    # https://docs.aws.amazon.com/codedeploy/latest/APIReference/API_TargetGroupPairInfo.html
    target_group_pair_info {
      # The path used by a load balancer to route production traffic when an Amazon ECS deployment is complete.
      prod_traffic_route {
        listener_arns = [aws_lb_listener.lb_listener.arn]
      }

      # One pair of target groups. One is associated with the original task set.
      # The second target is associated with the task set that serves traffic after the deployment completes.
      target_group {
        name = aws_lb_target_group.lb_target_group_1.name
      }
      target_group {
        name = aws_lb_target_group.lb_target_group_2.name
      }
    }
  }
}

# ECS AWS CodeDeploy IAM Role
#
# https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/codedeploy_IAM_role.html

# https://www.terraform.io/docs/providers/aws/r/iam_role.html
resource "aws_iam_role" "codedeploy_IAM_role" {
  name               = "${var.codedeploy_IAM_role_name}"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role_policy.json}"
  path               = "${var.iam_path}"
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

# https://www.terraform.io/docs/providers/aws/r/iam_policy.html
resource "aws_iam_policy" "ecs_policy" {
  name        = "${var.codedeploy_IAM_role_name}"
  policy      = "${data.aws_iam_policy_document.policy_document.json}"
  path        = "${var.iam_path}"
}

data "aws_iam_policy_document" "policy_document" {
  # If the tasks in your Amazon ECS service using the blue/green deployment type require the use of
  # the task execution role or a task role override, then you must add the iam:PassRole permission
  # for each task execution role or task role override to the AWS CodeDeploy IAM role as an inline policy.
  statement {
    effect = "Allow"

    actions = [
      "iam:PassRole",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ecs:DescribeServices",
      "ecs:CreateTaskSet",
      "ecs:UpdateServicePrimaryTaskSet",
      "ecs:DeleteTaskSet",
      "cloudwatch:DescribeAlarms",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "sns:Publish",
    ]

    resources = ["arn:aws:sns:*:*:CodeDeployTopic_*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:DescribeRules",
      "elasticloadbalancing:ModifyRule",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "lambda:InvokeFunction",
    ]

    resources = ["arn:aws:lambda:*:*:function:CodeDeployHook_*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetObjectMetadata",
      "s3:GetObjectVersion",
    ]

    condition {
      test     = "StringEquals"
      variable = "s3:ExistingObjectTag/UseWithCodeDeploy"
      values   = ["true"]
    }

    resources = ["*"]
  }
}

# https://www.terraform.io/docs/providers/aws/r/iam_role_policy_attachment.html
resource "aws_iam_role_policy_attachment" "default" {
  role       = "${aws_iam_role.codedeploy_IAM_role.name}"
  policy_arn = "${aws_iam_policy.ecs_policy.arn}"
}