resource "aws_iam_role" "aws_iam_role_codepipeline" {
    name = "thanhhv18-ojt-codepipeline-role"

    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = "thanhhv18-ojt-codepipeline-policy"
  role = aws_iam_role.aws_iam_role_codepipeline.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketVersioning",
        "s3:PutObjectAcl",
        "s3:PutObject"
      ],
      "Resource": [
        "arn:aws:s3:::thanhhv18"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "codebuild:BatchGetBuilds",
        "codebuild:StartBuild"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}


resource "aws_codepipeline" "aws_codepipeline" {
    name = "thanhhv18-ojt-codepipeline-test"
    role_arn = "${aws_iam_role.aws_iam_role_codepipeline.arn}"

    artifact_store {
        location = "thanhhv18"
        type = "S3"
    }

    stage {
        name = "Source"

        action {
        name             = "Source"
        category         = "Source"
        owner            = "AWS"
        provider         = "CodeCommit"
        version         = "1"
        #output_artifacts = ["src"]

        configuration = {
            RepositoryName = "thanhhv18" //MUST BE the name of the your codecommit repo
            BranchName = "master"
        }

        run_order = "1"
        }
    }

    stage {
      name = "Build"

      action {
        name             = "Build"
        category         = "Build"
        owner            = "AWS"
        provider         = "CodeBuild"
        version          = "1"
        #input_artifacts  = ["src"]
        output_artifacts = ["build"]

        configuration = {
          ProjectName = "thanhhv18-ojt-project-codebuild"
        }
      }
    }

    stage {
        name = "Deploy"

        action {
            name            = "Deploy"
            category        = "Deploy"
            owner           = "AWS"
            provider        = "CodeDeployToECS"
            version         = "1"
            input_artifacts = ["build"]

            configuration = {
                applicationName = "thanhhv18-ojt-project-codedeploy"
                deployment_group_name = "thanhhv18-ojt-project-codedeploy-group"
                taskDefinitionTemplate = "taskdef.json"
                appSpecTemplate = "appspec.yml"
                FileName    = "thanhhv18-ojt-project-ecs-taskdef.json"
            }
        }
    }
}
