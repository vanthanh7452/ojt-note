resource "aws_vpc" "main_vpc" {
  cidr_block = "${var.vpc_main_cidr}"
  instance_tenancy = "default"
  tags = {
    "Name" = "${var.vpc_name}"
    "Environment" = "${var.environment}"
  }
}

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    "Name" = "${var.igw_name}"
    "Environment" = "${var.environment}"
  }
}

resource "aws_subnet" "PublicSubnet_1" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "${var.public_subnet_cidr_1}"
  availability_zone = "${var.availability_zone_1}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"
  tags = {
    "Name" = "${var.public_subnet_1_name}"
    "Environment" = "${var.environment}"
  }
}

resource "aws_subnet" "PublicSubnet_2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "${var.public_subnet_cidr_2}"
  availability_zone = "${var.availability_zone_2}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"
  tags = {
    "Name" = "${var.public_subnet_2_name}"
    "Environment" = "${var.environment}"
  }
}

resource "aws_route_table" "Public_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.IGW.id
  }
  tags = {
    "Name" = "${var.route_table_name}"
    "Environment" = "${var.environment}"
  }
}

resource "aws_route_table_association" "rt_asso_public_1" {
  subnet_id = aws_subnet.PublicSubnet_1.id
  route_table_id = aws_route_table.Public_route_table.id
}
  
resource "aws_route_table_association" "rt_asso_public_2" {
  subnet_id = aws_subnet.PublicSubnet_2.id
  route_table_id = aws_route_table.Public_route_table.id
}

resource "aws_security_group" "security_group" {
  name = "${var.security_group_name}"
  description = "Auto create with Terraform"
  vpc_id = aws_vpc.main_vpc.id
  ingress {
    description = "TLS from VPC"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP from VPC"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH from VPC"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "${var.security_group_name}"
    "Environment" = "${var.environment}"
  }
}