output "vpc_id" {
  value = aws_vpc.main_vpc.id
}
output "public_subnet_1_id" {
  value = aws_subnet.PublicSubnet_1.id
}
output "public_subnet_2_id" {
  value = aws_subnet.PublicSubnet_2.id
}
output "security_group_id" {
  value = aws_security_group.security_group.id
}
