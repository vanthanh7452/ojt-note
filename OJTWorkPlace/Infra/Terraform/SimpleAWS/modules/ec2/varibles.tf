variable "ami" {}
variable "instance_type" {}
variable "key_name" {}
variable "subnet_id" {}
variable "env" {}
variable "public_key_path" {}
variable "sg_id" {}
variable "instance_count" {}