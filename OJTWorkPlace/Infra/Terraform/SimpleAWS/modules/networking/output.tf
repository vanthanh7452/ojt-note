output "vpc_id" {
  value = aws_vpc.terra_vpc.id
}
output "subnet_id" {
  value = aws_subnet.terra_public_subnet.id
}
output "sg_id" {
  value = aws_security_group.terra_sg.id
}