resource "aws_vpc" "terra_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = var.vpc_name
    Environment = var.env
  }
}

resource "aws_subnet" "terra_public_subnet" {
  vpc_id = aws_vpc.terra_vpc.id
  cidr_block = var.subnet_cidr
  availability_zone = var.subnet_az
  tags = {
    Name = var.subnet_name
    Environment = var.env
  }
}

resource "aws_internet_gateway" "terra_igw" {
  vpc_id = aws_vpc.terra_vpc.id
  tags = {
    Name = var.igw_name
    Environment = var.env
  }
}

resource "aws_route_table" "terra_public_route_table" {
  vpc_id = aws_vpc.terra_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terra_igw.id
  }
  tags = {
    Name = var.route_table_name
    Environment = var.env
    }
}

resource "aws_route_table_association" "terra_public_route_table_association" {
  subnet_id = aws_subnet.terra_public_subnet.id
  route_table_id = aws_route_table.terra_public_route_table.id
}

resource "aws_security_group" "terra_sg" {
    name = var.sg_name
    vpc_id = aws_vpc.terra_vpc.id
    tags = {
        Name = var.sg_name
        Environment = var.env
    }
    ingress {
        description = "Allow SSH"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        description = "Allow HTTP"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        description = "Allow HTTPS"
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }   
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}