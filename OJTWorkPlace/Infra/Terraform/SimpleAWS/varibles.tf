variable "vpc_name" {
    type = string
    description = "value for vpc's name"
}
variable "vpc_cidr" {
    type = string
    description = "value for vpc's cidr"
}
variable "subnet_name" {
    type = string
    description = "value for subnet's name"
}
variable "subnet_cidr" {
    type = string
    description = "value for subnet's cidr"
}
variable "subnet_az" {
    type = string
    description = "value for subnet's availability zone"
}
variable "igw_name" {
    type = string
    description = "value for igw's name"
}
variable "route_table_name" {
    type = string
    description = "value for route table's name"
}
variable "sg_name" {
    type = string
    description = "value for security group's name"
}
variable "env" {
    type = string
    description = "value for environment"
}
variable "ami" {
    type = string
    description = "value for ami"
}
variable "instance_type" {
    type = string
    description = "value for instance type"
}
variable "key_name" {
    type = string
    description = "value for key name"
}
variable "public_key_path" {
    type = string
    description = "value for public key path"
}
variable "instance_count" {
    type = number
    description = "value for count"
}